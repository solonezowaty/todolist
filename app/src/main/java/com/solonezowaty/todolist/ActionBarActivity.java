package com.solonezowaty.todolist;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

/**
 * Created by Jakub Solecki on 21.11.2018.
 */
public abstract class ActionBarActivity extends AppCompatActivity {

    protected abstract boolean getBackButtonEnabled();

    protected abstract String getActionBarTitle();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle(getActionBarTitle());
        getSupportActionBar().setDisplayHomeAsUpEnabled(getBackButtonEnabled());
        getSupportActionBar().setElevation(0);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:{
                finish();
                break;
            }
        }
        return true;
    }
}
