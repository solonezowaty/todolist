package com.solonezowaty.todolist.add_new_task.view;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import com.solonezowaty.todolist.ActionBarActivity;
import com.solonezowaty.todolist.R;
import com.solonezowaty.todolist.add_new_task.view_model.AddNewTaskViewModel;
import com.solonezowaty.todolist.add_new_task.view_model.AddNewTaskViewModelFactory;
import com.solonezowaty.todolist.list_screen.view.TaskListActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddNewTaskActivity extends ActionBarActivity {

    @BindView(R.id.task_title_edit_text) EditText taskTitleEditText;
    @BindView(R.id.completed_task_checkbox) CheckBox completedTaskCheckbox;

    private AddNewTaskViewModel addNewTaskViewModel;
    private boolean isCompletedTask;

    @Override
    protected boolean getBackButtonEnabled() {
        return true;
    }

    @Override
    protected String getActionBarTitle() {
        return getString(R.string.add_new_task_button_text);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_task);

        ButterKnife.bind(this);

        addNewTaskViewModel = ViewModelProviders.of(this, new AddNewTaskViewModelFactory()).get(AddNewTaskViewModel.class);

        completedTaskCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isCompletedTask = isChecked;
            }
        });
    }

    @OnClick(R.id.add_new_task_button)
    public void addNewTaskButtonClick(){
        addNewTaskViewModel.addNewTask(AddNewTaskActivity.this, taskTitleEditText.getText().toString(), isCompletedTask);
        Intent intent = new Intent(AddNewTaskActivity.this, TaskListActivity.class);
        startActivity(intent);
        finish();
    }
}
