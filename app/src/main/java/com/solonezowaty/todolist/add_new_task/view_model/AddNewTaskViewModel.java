package com.solonezowaty.todolist.add_new_task.view_model;

import android.arch.lifecycle.ViewModel;
import android.content.Context;

import com.solonezowaty.todolist.models.TaskModel;
import com.solonezowaty.todolist.room_database.TaskDatabase;

/**
 * Created by Jakub Solecki on 26.11.2018.
 */
public class AddNewTaskViewModel extends ViewModel {

    public void addNewTask(Context context, String title, boolean isCompleted){
        TaskModel newTask = new TaskModel();
        newTask.setUserId(1);
        newTask.setTitle(title);
        newTask.setCompleted(isCompleted);
        TaskDatabase.getDatabaseInstance(context)
                .taskDao().insertTask(newTask);
    }
}
