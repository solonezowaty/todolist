package com.solonezowaty.todolist.add_new_task.view_model;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.NonNull;

import com.solonezowaty.todolist.list_screen.view_model.TaskListViewModel;

/**
 * Created by Jakub Solecki on 26.11.2018.
 */
public class AddNewTaskViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    public AddNewTaskViewModelFactory() {
    }

    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new AddNewTaskViewModel();
    }
}
