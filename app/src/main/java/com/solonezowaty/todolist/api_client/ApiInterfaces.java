package com.solonezowaty.todolist.api_client;

import com.solonezowaty.todolist.models.TaskModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Jakub Solecki on 21.11.2018.
 */
public interface ApiInterfaces {

    @GET("/todos/")
    Call<List<TaskModel>> getItemsList();
}
