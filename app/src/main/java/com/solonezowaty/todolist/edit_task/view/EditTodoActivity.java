package com.solonezowaty.todolist.edit_task.view;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.EditText;

import com.solonezowaty.todolist.ActionBarActivity;
import com.solonezowaty.todolist.R;
import com.solonezowaty.todolist.edit_task.view_model.EditTodoViewModel;
import com.solonezowaty.todolist.edit_task.view_model.EditTodoViewModelFactory;
import com.solonezowaty.todolist.list_screen.view.TaskListActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditTodoActivity extends ActionBarActivity {

    @BindView(R.id.title_todo_edit_text) EditText todoTitle;
    @BindView(R.id.completed_task_checkbox) CheckBox completedCheckbox;

    Intent intent;
    private EditTodoViewModel editTodoViewModel;

    @Override
    protected boolean getBackButtonEnabled() {
        return true;
    }

    @Override
    protected String getActionBarTitle() {
        return getString(R.string.edit_todo_button_text);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_todo);

        ButterKnife.bind(this);
        editTodoViewModel = ViewModelProviders.of(this, new EditTodoViewModelFactory()).get(EditTodoViewModel.class);
        intent = getIntent();

        int localDbId = intent.getIntExtra("localDbId", 0);
        setDataToUiToEdit(localDbId);
    }

    @OnClick(R.id.edit_task_button)
    public void editTodoButtonClick(){
        String title = todoTitle.getText().toString();
        boolean isCompleted = completedCheckbox.isChecked();
        editTodoViewModel.saveDataToEditTodo(EditTodoActivity.this, title, isCompleted);

        Intent intent = new Intent(EditTodoActivity.this, TaskListActivity.class);
        startActivity(intent);
        finish();
    }

    private void setDataToUiToEdit(int localDbId){
        todoTitle.setText(editTodoViewModel.getDataToEditSelectedTodo(EditTodoActivity.this, localDbId).getTitle());

        if (editTodoViewModel.getDataToEditSelectedTodo(EditTodoActivity.this, localDbId).isCompleted()){
            completedCheckbox.setChecked(true);
        } else {
            completedCheckbox.setChecked(false);
        }
    }
}
