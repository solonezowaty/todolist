package com.solonezowaty.todolist.edit_task.view_model;

import android.arch.lifecycle.ViewModel;
import android.content.Context;

import com.solonezowaty.todolist.models.TaskModel;
import com.solonezowaty.todolist.room_database.TaskDatabase;

/**
 * Created by Jakub Solecki on 27.11.2018.
 */
public class EditTodoViewModel extends ViewModel {

    private TaskModel taskModel;

    public TaskModel getDataToEditSelectedTodo(Context context, int localDbId){
        taskModel = TaskDatabase.getDatabaseInstance(context).taskDao().getSelectedTodoToEdit(localDbId);

        return taskModel;
    }

    public void saveDataToEditTodo(Context context, String title, boolean isCompleted){
        taskModel.setTitle(title);
        taskModel.setCompleted(isCompleted);
        TaskDatabase.getDatabaseInstance(context).taskDao().update(taskModel);
    }
}
