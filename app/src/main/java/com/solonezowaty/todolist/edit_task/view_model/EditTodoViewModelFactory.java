package com.solonezowaty.todolist.edit_task.view_model;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.solonezowaty.todolist.add_new_task.view_model.AddNewTaskViewModel;

/**
 * Created by Jakub Solecki on 27.11.2018.
 */
public class EditTodoViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    public EditTodoViewModelFactory() {
    }

    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new EditTodoViewModel();
    }
}
