package com.solonezowaty.todolist.list_screen.view;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.solonezowaty.todolist.R;
import com.solonezowaty.todolist.add_new_task.view.AddNewTaskActivity;
import com.solonezowaty.todolist.list_screen.TaskInterface;
import com.solonezowaty.todolist.list_screen.view_model.TaskListViewModel;
import com.solonezowaty.todolist.list_screen.view_model.TaskListViewModelFactory;
import com.solonezowaty.todolist.models.TaskModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;

public class TaskListActivity extends AppCompatActivity implements TaskInterface {

    @BindView(R.id.search_list_edittext) EditText searchEditText;
    @BindView(R.id.task_list_recycler_view) RecyclerView recyclerView;
    @BindView(R.id.filter_list_button) Spinner spinner;
    @BindView(R.id.empty_searching_result_text_view) View emptySearchResult;

    private RecyclerView.Adapter mAdapter;
    private TaskListViewModel taskListViewModel;
    private List<TaskModel> taskModelArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        ButterKnife.bind(this);

        taskListViewModel = ViewModelProviders.of(this, new TaskListViewModelFactory()).get(TaskListViewModel.class);
        taskListViewModel.addNothingMatchInterface(this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.filter_list, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    @Override
    protected void onResume(){
        super.onResume();
        populateRecyclerView(this);
    }

    @Override
    protected void onPause(){
        super.onPause();
        taskListViewModel.removeInterfaces();
    }

    @OnItemSelected(R.id.filter_list_button)
    public void onItemSelected(){
        String selectedItem = spinner.getSelectedItem().toString();

        if (selectedItem.matches("Wszystkie")){
            taskListViewModel.getFilteredListWithAllTasks(this);
        } else if (selectedItem.matches("Ukończone")){
            taskListViewModel.getFilteredListWithCompletedTasks(this);
        } else if (selectedItem.matches("Nieukończone")){
            taskListViewModel.getFilteredListWithNotCompletedTasks(this);
        }
        if (recyclerView.getVisibility() == View.GONE){
            recyclerView.setVisibility(View.VISIBLE);
            emptySearchResult.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.add_new_task_button)
    public void addNewTaskButtonClick(){
        Intent intent = new Intent(TaskListActivity.this, AddNewTaskActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.search_todo_button)
    public void searchButtonClick(){
        String searchedPrase = searchEditText.getText().toString();
        taskListViewModel.startSearch(this, searchedPrase);
        populateRecyclerView(this);
    }

    public void populateRecyclerView(final Context context){
        Observer<List<TaskModel>> todosList = new Observer<List<TaskModel>>() {
            @Override
            public void onChanged(@Nullable List<TaskModel> taskModels) {
                taskListViewModel.shouldGetListFromApi(context);

                taskModelArrayList = taskModels;
                mAdapter = new TaskListRecyclerAdapter(taskModelArrayList, TaskListActivity.this);
                recyclerView.setAdapter(mAdapter);
            }
        };
        taskListViewModel.shouldGetListFromApi(context).observe(this, todosList);
    }

    @Override
    public void nothingMatch() {
        recyclerView.setVisibility(View.GONE);
        emptySearchResult.setVisibility(View.VISIBLE);
    }
}
