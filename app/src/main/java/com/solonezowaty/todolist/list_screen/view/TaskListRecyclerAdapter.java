package com.solonezowaty.todolist.list_screen.view;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.solonezowaty.todolist.R;
import com.solonezowaty.todolist.edit_task.view.EditTodoActivity;
import com.solonezowaty.todolist.models.TaskModel;
import com.solonezowaty.todolist.room_database.TaskDatabase;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Jakub Solecki on 22.11.2018.
 */
public class TaskListRecyclerAdapter extends RecyclerView.Adapter<TaskListRecyclerAdapter.ViewHolder> {

    private List<TaskModel> itemsList;
    private Context context;

    public TaskListRecyclerAdapter(List<TaskModel> itemsList, Context context) {
        this.itemsList = itemsList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int position) {
        final TaskModel listElementsModel = itemsList.get(position);
        viewHolder.taskTitleText.setText(listElementsModel.getTitle());
        if (!listElementsModel.isCompleted()){
            viewHolder.taskCompletedCheckbox.setChecked(false);
        } else {
            viewHolder.taskCompletedCheckbox.setChecked(true);
        }

        viewHolder.editTaskButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int localDbId = itemsList.get(position).getLocalDbId();
                Intent intent = new Intent(v.getContext().getApplicationContext(), EditTodoActivity.class);
                intent.putExtra("localDbId", localDbId);
                v.getContext().startActivity(intent);
            }
        });

        viewHolder.deleteTaskButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("Czy na pewno chcesz usunąć zadanie?")
                        .setTitle("Usuń");
                builder.setPositiveButton("TAK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        itemsList.get(position);
                        itemsList.remove(position);
                        TaskDatabase.getDatabaseInstance(context).taskDao().delete(listElementsModel);
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position, itemsList.size());
                    }
                });
                builder.setNegativeButton("NIE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
                builder.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.task_title_text) TextView taskTitleText;
        @BindView(R.id.edit_task_button) View editTaskButton;
        @BindView(R.id.delete_task_button) View deleteTaskButton;
        @BindView(R.id.task_completed_checkbox) CheckBox taskCompletedCheckbox;

        public ViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }
}
