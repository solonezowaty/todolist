package com.solonezowaty.todolist.list_screen.view_model;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.solonezowaty.todolist.api_client.ApiClient;
import com.solonezowaty.todolist.api_client.ApiInterfaces;
import com.solonezowaty.todolist.list_screen.TaskInterface;
import com.solonezowaty.todolist.models.TaskModel;
import com.solonezowaty.todolist.room_database.TaskDatabase;
import com.solonezowaty.todolist.utils.SharedPreferencesUtils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Jakub Solecki on 21.11.2018.
 */
public class TaskListViewModel extends ViewModel {

    private MutableLiveData<List<TaskModel>> todoList;
    private AsyncTask<Void, Void, List<TaskModel>> asyncListLoading;

    private TaskInterface taskInterface;

    public MutableLiveData<List<TaskModel>> shouldGetListFromApi(Context context){
        if (TaskDatabase.getDatabaseInstance(context).taskDao().getAllTasksList().size() == 0
                && SharedPreferencesUtils.getBooleanFromSharedPreferences(context, SharedPreferencesUtils.DOWNLOAD_TODO_LIST_ENABLED)){
             if (todoList == null){
                 todoList = new MutableLiveData<>();
                 getItemsFromApi(context);
                 SharedPreferencesUtils.setFilteredListTypePicked(context, SharedPreferencesUtils.FILTERED_LIST_TYPE_PICKED, "all");
             }
        } else {
            if (todoList == null){
                todoList = new MutableLiveData<>();
                typeOfFilterInTodoList(context);
            }
        }
        return todoList;
    }

    private void typeOfFilterInTodoList(Context context){
        if (SharedPreferencesUtils.getFilteredTypeFromSharedPreferences(context, SharedPreferencesUtils.FILTERED_LIST_TYPE_PICKED).matches("all")){
            todoList.setValue(TaskDatabase
                    .getDatabaseInstance(context)
                    .taskDao()
                    .getAllTasksList());

        } else if (SharedPreferencesUtils.getFilteredTypeFromSharedPreferences(context, SharedPreferencesUtils.FILTERED_LIST_TYPE_PICKED).matches("completed")){
            todoList.setValue(TaskDatabase.
                    getDatabaseInstance(context)
                    .taskDao()
                    .getFilteredTasksList(true));

        } else if (SharedPreferencesUtils.getFilteredTypeFromSharedPreferences(context, SharedPreferencesUtils.FILTERED_LIST_TYPE_PICKED).matches("not_completed")){
            todoList.setValue(TaskDatabase
                    .getDatabaseInstance(context)
                    .taskDao()
                    .getFilteredTasksList(false));
        }
    }

    public void startSearch(Context context, String searchedPrase){
        todoList = new MutableLiveData<>();
        if (searchedPrase.matches("")){
            taskInterface.nothingMatch();
        }else {
            try {
                int userId = Integer.parseInt(searchedPrase);
                if (TaskDatabase.getDatabaseInstance(context).taskDao().getTodoListWithSpecificUserId(userId).size() == 0){
                    taskInterface.nothingMatch();
                } else {
                    shouldGetListFromApi(context).setValue(TaskDatabase.getDatabaseInstance(context).taskDao().getTodoListWithSpecificUserId(userId));
                }
            } catch (NumberFormatException e){
                if (TaskDatabase.getDatabaseInstance(context).taskDao().getTodoListWithSpecificTitle(searchedPrase).size() == 0){
                    taskInterface.nothingMatch();
                } else {
                    shouldGetListFromApi(context).setValue(TaskDatabase.getDatabaseInstance(context).taskDao().getTodoListWithSpecificTitle(searchedPrase));
                }
            }
        }
    }

    private void getItemsFromApi(final Context context){
        ApiInterfaces apiInterfaces = ApiClient.getRetrofit().create(ApiInterfaces.class);
        Call<List<TaskModel>> call = apiInterfaces.getItemsList();
        call.enqueue(new Callback<List<TaskModel>>() {
            @Override
            public void onResponse(Call<List<TaskModel>> call, Response<List<TaskModel>> response) {
                if (response.isSuccessful() && response.code() == 200){
                    Log.e("RESPONSE: ", "Tu jest reponse: " + response.body());
                    List<TaskModel> todoListFromApi = response.body();

                    putTodoListFromApiIntoDatabase(context, todoListFromApi);

                    SharedPreferencesUtils.setBooleanToSharedPreferences(context, SharedPreferencesUtils.DOWNLOAD_TODO_LIST_ENABLED, false);
                }
            }

            @Override
            public void onFailure(Call<List<TaskModel>> call, Throwable t) {

            }
        });
    }

    private void putTodoListFromApiIntoDatabase(final Context context, final List<TaskModel> todoListFromApi){
        asyncListLoading = new AsyncTask<Void, Void, List<TaskModel>>() {
            @Override
            protected List<TaskModel> doInBackground(Void... voids) {

                for (int i = 0; i < todoListFromApi.size(); i++){
                    TaskDatabase.getDatabaseInstance(context).taskDao().insertTask(todoListFromApi.get(i));
                }

                return TaskDatabase.getDatabaseInstance(context).taskDao().getAllTasksList();
            }

            @Override
            protected void onPostExecute(List<TaskModel> todoListFromApi) {
                todoList.setValue(todoListFromApi);
            }
        };
        asyncListLoading.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        if(asyncListLoading != null){
            asyncListLoading.cancel(true);
        }
    }

    public void getFilteredListWithAllTasks(Context context){
        SharedPreferencesUtils.setFilteredListTypePicked(context, SharedPreferencesUtils.FILTERED_LIST_TYPE_PICKED, "all");
        shouldGetListFromApi(context).setValue(TaskDatabase.getDatabaseInstance(context).taskDao().getAllTasksList());
    }

    public void getFilteredListWithCompletedTasks(Context context){
        SharedPreferencesUtils.setFilteredListTypePicked(context, SharedPreferencesUtils.FILTERED_LIST_TYPE_PICKED, "completed");
        shouldGetListFromApi(context).setValue(TaskDatabase.getDatabaseInstance(context).taskDao().getFilteredTasksList(true));
    }

    public void getFilteredListWithNotCompletedTasks(Context context){
        SharedPreferencesUtils.setFilteredListTypePicked(context, SharedPreferencesUtils.FILTERED_LIST_TYPE_PICKED, "not_completed");
        shouldGetListFromApi(context).setValue(TaskDatabase.getDatabaseInstance(context).taskDao().getFilteredTasksList(false));
    }

    public void addNothingMatchInterface(TaskInterface taskInterface) {
        this.taskInterface = taskInterface;
    }

    public void removeInterfaces() {
        taskInterface = null;
    }
}
