package com.solonezowaty.todolist.list_screen.view_model;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

/**
 * Created by Jakub Solecki on 21.11.2018.
 */
public class TaskListViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    public TaskListViewModelFactory() {
    }

    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new TaskListViewModel();
    }
}
