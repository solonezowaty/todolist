package com.solonezowaty.todolist.models;

import com.google.gson.annotations.Expose;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by Jakub Solecki on 21.11.2018.
 */

@Entity
public class TaskModel {

    @PrimaryKey(autoGenerate = true)
    private int localDbId;

    @Expose
    private int userId;
    @Expose
    private int id;
    @Expose
    private String title;
    @Expose
    private boolean completed;

    public TaskModel() {
    }

    public int getLocalDbId() {
        return localDbId;
    }

    public void setLocalDbId(int localDbId) {
        this.localDbId = localDbId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    @Override
    public String toString() {
        return "{" +
                "userId=" + userId +
                ", id=" + id +
                ", title='" + title + '\'' +
                ", completed=" + completed +
                '}';
    }
}
