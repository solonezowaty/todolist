package com.solonezowaty.todolist.room_database;

import com.solonezowaty.todolist.models.TaskModel;

import java.util.List;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

/**
 * Created by Jakub Solecki on 22.11.2018.
 */

@Dao
public interface TaskDao {

    @Insert
    void insertTask(TaskModel taskModel);

    @Query("SELECT * FROM TaskModel")
    List<TaskModel> getAllTasksList();

    @Query("SELECT * FROM TaskModel WHERE completed = :completed")
    List<TaskModel> getFilteredTasksList(boolean completed);

    @Query("SELECT * FROM TaskModel WHERE localDbId = :localDbId")
    TaskModel getSelectedTodoToEdit(int localDbId);

    @Query("SELECT * FROM TaskModel WHERE userId = :userId")
    List<TaskModel> getTodoListWithSpecificUserId(int userId);

    @Query("SELECT * FROM TaskModel WHERE title = :title")
    List<TaskModel> getTodoListWithSpecificTitle(String title);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(TaskModel taskModel);

    @Delete
    void delete(TaskModel taskModel);
}
