package com.solonezowaty.todolist.room_database;

import android.content.Context;

import com.solonezowaty.todolist.models.TaskModel;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;

/**
 * Created by Jakub Solecki on 22.11.2018.
 */
@Database(entities = {TaskModel.class}, version = 1, exportSchema = false)
public abstract class TaskDatabase extends RoomDatabase {

    public abstract TaskDao taskDao();
    public static TaskDatabase INSTANCE;

    public static TaskDatabase getDatabaseInstance(final Context context){
        if (INSTANCE == null){
            synchronized (TaskDatabase.class){
                if (INSTANCE == null){
                    INSTANCE = Room
                            .databaseBuilder(context.getApplicationContext(), TaskDatabase.class, "task_database")
                            .allowMainThreadQueries().build();
                }
            }
        }
        return INSTANCE;
    }
}
