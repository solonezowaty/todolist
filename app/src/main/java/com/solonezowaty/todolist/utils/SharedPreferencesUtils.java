package com.solonezowaty.todolist.utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.sql.Struct;

/**
 * Created by Jakub Solecki on 2018-11-24.
 */
public class SharedPreferencesUtils {

    private static final String PREFERENCES_KEY = "pref_key" + SharedPreferencesUtils.class.getName();
    public static String DOWNLOAD_TODO_LIST_ENABLED = PREFERENCES_KEY + "DOWNLOAD_TODO_LIST_ENABLED";
    public static String FILTERED_LIST_TYPE_PICKED = PREFERENCES_KEY + "FILTERED_LIST_TYPE_PICKED";


    public static boolean getBooleanFromSharedPreferences(Context context, String tag) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFERENCES_KEY, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(tag, true);
    }

    public static void setBooleanToSharedPreferences(Context context, String tag, boolean booleanToPut) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFERENCES_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(tag, booleanToPut);
        editor.commit();
    }

    public static String getFilteredTypeFromSharedPreferences(Context context, String tag){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFERENCES_KEY, Context.MODE_PRIVATE);
        return sharedPreferences.getString(tag, "all");
    }

    public static void setFilteredListTypePicked(Context context, String tag, String filteredPicked){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFERENCES_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(tag, filteredPicked);
        editor.commit();
    }
}
