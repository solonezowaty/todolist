package com.solonezowaty.todolist.welcome_screen;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.solonezowaty.todolist.R;
import com.solonezowaty.todolist.list_screen.view.TaskListActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.begin_button) View beginButton;
    @BindView(R.id.welcome_screen_layout) View welcomeScreen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        setOnClickListeners();

        setAnimations();
    }

    private void setOnClickListeners(){
        beginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, TaskListActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private void setAnimations(){
        Animation fadeInAnimation = AnimationUtils.loadAnimation(this, R.anim.fade_in_animation);
        welcomeScreen.startAnimation(fadeInAnimation);

        Animation pulse = AnimationUtils.loadAnimation(this, R.anim.pulse);
        beginButton.startAnimation(pulse);
    }
}
